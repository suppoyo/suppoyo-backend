const errors = require('../errors.json');

module.exports = (req, res, next) => {
    if (!req.session.account || !req.session.profile) return next(errors.LOGIN_REQUIRED);
    next();
};