const cors = require('cors');
const express = require('express');
const session = require('express-session');

const errorHandler = require('./middlewares/errorHandler');
const isAuthenticated = require('./middlewares/isAuthenticated');

const errors = require('./errors.json');
const port = process.env.PORT || 3000;

const accountRouter = require('./routes/account');
const profileRouter = require('./routes/profile');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const app = express();

// Cross-Origin-Resource-Sharing support
app.use(cors({
    credentials: true,
    /**
     * Handles the origin for CORS request
     * @param {String} origin the origin
     * @param {Function} callback callback function
     */
    origin: (origin, callback) => {
        if (!origin || origin === 'null') origin = '*';
        callback(null, origin);
    }
}));

// route parsing
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

// session handling
app.use(session({
    secret: process.env.SESSION_SECRET || 'session-secret',
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: process.env.NODE_ENV === 'production'
    }
}));

// route handling
app.use('/account', accountRouter);
app.use('/profiles', isAuthenticated, profileRouter);


// unknown route
app.use((_req, _res, next) => next(errors.UNKNOWN_ROUTE));

// error handling
app.use(errorHandler);

// initialization
app.listen(port, () => console.log(`[HTTP] Server started on port ${port}`));

module.exports = app;