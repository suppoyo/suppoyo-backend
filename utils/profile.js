const db = require('../utils/db');

const getProfileById = async (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const foundProfile = (await db.query('SELECT * FROM profiles WHERE id=?', [id]))[0];
            
            resolve(foundProfile);
        } catch (error) {
            reject(error);
        }
    });
};

const getProfileFromAccount = async (account) => {
    return new Promise(async (resolve, reject) => {
        try {
            const foundProfile = (await db.query('SELECT * FROM profiles WHERE account=?', [account]))[0];
            
            resolve(foundProfile);
        } catch (error) {
            reject(error);
        }
    });
};

module.exports = {
    getProfileFromAccount,
    getProfileById
};