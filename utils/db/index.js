const mysql = require('mysql');

const errors = require('../../errors.json');

const db = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PWD,
    database: process.env.DB_NAME
});

module.exports = {
    /**
     * Queries the database with given sql string containing placeholders and it's parameters
     * It automatically escapes the sql string through binding the corresponding parameters
     * @example const result = await db.query('SELECT lastname FROM users WHERE firstname=?', ['Jon']);;
     * @param {String} sql the sql string
     * @param {Array|Object} params Array of values or Object to bind to parameters ('?')
     * @returns {Promise}
     */
    query: (sql, params) => {
        return new Promise((resolve, reject) => {
            if (!Array.isArray(params) && params == null) params = [];
            if (typeof sql !== 'string') return reject(errors.INVALID_PARAMETERS);
            db.query(mysql.format(sql, params), (err, dbRes) => {
                if (err) return reject(errors.DB_ERROR);
                resolve(dbRes);
            });
        });
    }
};