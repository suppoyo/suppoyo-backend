const db = require('../utils/db');

const getAccountByID = async (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const foundAccount = (await db.query('SELECT * FROM accounts WHERE id=?', [id]))[0];
            
            resolve(foundAccount);
        } catch (error) {
            reject(error);
        }
    });
};

const getAccountByMail = async (mail) => {
    return new Promise(async (resolve, reject) => {
        try {
            const foundAccount = (await db.query('SELECT * FROM accounts WHERE mail=?', [mail]))[0];
            
            resolve(foundAccount);
        } catch (error) {
            reject(error);
        }
    });
};

const isValidMail = (mail) => /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail);

module.exports = {
    getAccountByID,
    getAccountByMail,
    isValidMail
};