const asyncHandler = require('../utils/asyncHandler');
const db = require('../utils/db');
const errors = require('../errors.json');
const profileUtils = require('../utils/profile');

const sanitizeProfile = (profile) => {
    const newProfile = {};
    const allowedKeys = ['firstname', 'lastname', 'street', 'streetno', 'zip', 'city', 'country', 'bio'];

    for (const key in profile) {
        if (profile.hasOwnProperty(key) && allowedKeys.includes(key)) {
            newProfile[key] = profile[key];
        }
    }
    return newProfile;
};

const getProfiles = asyncHandler(async (req, res, next) => {
    const profiles = await db.query('SELECT * FROM profiles');

    res.json(profiles);
});

const getProfile = asyncHandler(async (req, res, next) => {
    const profile = await profileUtils.getProfileById(req.params.id);

    if (!profile) return next(errors.PROFILE_NOT_FOUND);
    res.json(profile);
});

const updateProfile = asyncHandler(async (req, res, next) => {
    // check if profile to update is our own
    if (req.session.account.id != req.params.id) return next(errors.FORBIDDEN);
    const profile = sanitizeProfile(req.body);

    await db.query('UPDATE profiles SET ?', profile);
    req.session.profile = profile;
    res.json(profile);
});

module.exports = {
    getProfiles,
    getProfile,
    updateProfile
};