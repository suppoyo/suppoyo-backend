  
const bcrypt = require('bcrypt');

const asyncHandler = require('../utils/asyncHandler');
const db = require('../utils/db');
const errors = require('../errors.json');
const accountUtils = require('../utils/account');

const register = asyncHandler(async (req, res, next) => {
    // check password complexity and mail
    if (!accountUtils.isValidMail(req.body.mail)) return next(errors.INVALID_MAIL);
    if (!req.body.password || req.body.password.length < (process.env.PASSWORD_MIN_LENGTH || 6)) return next(errors.INVALID_PASSWORD);
    // ensure that account is not already taken
    if (await accountUtils.getAccountByMail(req.body.mail)) return next(errors.ACCOUNT_ALREADY_REGISTERED);
    // create hash, register account
    const account = {
        mail: req.body.mail,
        pwd_hash: await bcrypt.hash(req.body.password, 10)
    };
    const result = await db.query('INSERT INTO accounts SET ?', account);
    account.id = result.insertId;
    req.session.account = account;
    // create profile
    const profile = {
        account: account.id,
        helper: !!req.body.helper
    };
    await db.query('INSERT INTO profiles SET ?', profile);
    req.session.profile = profile;

    res.json({
        id: account.id
    });
});

const login = asyncHandler(async (req, res, next) => {
    // check password complexity and mail
    if (!accountUtils.isValidMail(req.body.mail)) return next(errors.INVALID_MAIL);
    if (!req.body.password || req.body.password.length < (process.env.PASSWORD_MIN_LENGTH || 6)) return next(errors.INVALID_PASSWORD);
    // check if account exists
    const account = await accountUtils.getAccountByMail(req.body.mail);

    if (!account) return next(errors.ACCOUNT_NOT_FOUND);
    // compare hash
    if (!(await bcrypt.compare(req.body.password, account.pwd_hash))) return next(errors.INVALID_CREDENTIALS);
    req.session.account = account;
    // get profile info
    req.session.profile = (await db.query('SELECT * FROM profiles WHERE account=?', [account.id]))[0];
    res.json({
        id: account.id
    });
});

module.exports = {
    register,
    login
};